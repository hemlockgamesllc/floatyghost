﻿using UnityEngine;

public interface InformWhenDead  {

	void ThisObjectDied( GameObject Obj );
}
