﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InformBecauseDieing : MonoBehaviour {

	private List<InformWhenDead> WhoWantsToKnow;

	void Start()
	{
		WhoWantsToKnow = new List<InformWhenDead>();
	}

	// Use this for initialization
	public void InformMe( InformWhenDead Obj) {
		WhoWantsToKnow.Add( Obj );
	}
	public void DontInformMe( InformWhenDead Obj ){
		WhoWantsToKnow.Remove( Obj );
	}
	
	// Update is called once per frame
	void OnDestroy()   
	{
		for( int i = 0; i < WhoWantsToKnow.Count ; i++ )
		{
			WhoWantsToKnow[i].ThisObjectDied( gameObject );
		}
	}
}
