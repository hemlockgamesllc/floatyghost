﻿using UnityEngine;
using System.Collections;

public class RocketForce : MonoBehaviour {
	private Rigidbody LocalRigidbody;
	private Transform LocalTransform;
	public float Force;

	void Start()
	{
		LocalRigidbody = rigidbody;
		LocalTransform = transform;
	}

	void FixedUpdate() {
		LocalRigidbody.AddForce(LocalTransform.up * Force);
	}
}
