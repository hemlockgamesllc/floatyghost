﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FirePooling : MonoBehaviour {

	public GameObject FirePrefab;
	static Transform[] FireObjPool;
	static readonly Vector3 HidePosition = new Vector3( -100,0,0 );
	static int CurrentFireIndex =-1;
	static int CurrentFirePoolSize = 50;

	void Start()
	{
		//FireObjPool = new List<GameObject>();
		FireObjPool = new Transform[CurrentFirePoolSize];
		for( int i = 0; i < FireObjPool.Length ; i++ )
		{
			FireObjPool[i] = ((GameObject)Instantiate( FirePrefab, HidePosition, Quaternion.identity )).transform;
			FireObjPool[i].GetComponent<PoolingObj>().PoolIndex = i;
			FireObjPool[i].GetComponent<PoolingObj>().DeactivatePollingObj();
		}
	}

	public static Transform ActivatePoolingObj()
	{
		CurrentFireIndex = (CurrentFireIndex + 1) % CurrentFirePoolSize;
		FireObjPool[CurrentFireIndex].GetComponent<PoolingObj>().ActivatePollingObj();
		return FireObjPool[CurrentFireIndex];
	}

	public static void DeactivatePoolingObj( int ObjIndex )
	{
		Transform Temp = FireObjPool[ObjIndex];
		Temp.position = HidePosition;
		Temp.parent = null;
		Temp.GetComponent<PoolingObj>().DeactivatePollingObj();
	} 

	
	public static void ObjDeactivatingSelf( Transform Trans )
	{
		Trans.position = HidePosition;
		Trans.parent = null;
	}
	 
}
