﻿using UnityEngine;
using System.Collections;

public abstract class PoolingObj : MonoBehaviour {

	public int PoolIndex;

	public  abstract void ActivatePollingObj ();
	
	public  abstract void DeactivatePollingObj ();
	
	public  abstract void DeactivateYourself ();

}
