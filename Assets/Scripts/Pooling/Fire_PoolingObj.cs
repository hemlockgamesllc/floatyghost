﻿using UnityEngine;
using System.Collections;

public class Fire_PoolingObj : PoolingObj {

	public ParticleSystem FireFX;
	public float DeathTime;

	public override void ActivatePollingObj ()
	{
		FireFX.enableEmission = true;
		StartCoroutine( "FireTimedDeath" );
	}
	
	public override void DeactivatePollingObj ()
	{
		FireFX.enableEmission = false;
		StopCoroutine( "FireTimedDeath" );
	}
	
	public override void DeactivateYourself ()
	{
		FireFX.enableEmission = false;
		StopCoroutine( "FireTimedDeath" );
		FirePooling.ObjDeactivatingSelf( transform );
	}

	IEnumerator FireTimedDeath()
	{
		yield return new WaitForSeconds( DeathTime );
		DeactivateYourself ();
	}
}
