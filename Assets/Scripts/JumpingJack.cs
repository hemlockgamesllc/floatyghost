﻿using UnityEngine;
using System.Collections;

public class JumpingJack : MonoBehaviour {
	private Rigidbody LocalRigidbody;
	private Transform LocalTransform;
	public float Force;
	public float RandomForce;

	
	public float angDrag;
	public float maxAngVel;

	public Vector3 RotationSpeed;

	
	public ParticleSystem PartSystem;
	public float TimeTillDeath;
 
	void Start()
	{
		LocalRigidbody = rigidbody;
		LocalTransform = transform;
		
		LocalRigidbody.maxAngularVelocity = maxAngVel;
		LocalRigidbody.angularDrag = angDrag;

		StartCoroutine( Death () );
	} 


	void FixedUpdate()
	{
		LocalTransform.Rotate( RotationSpeed );
		LocalRigidbody.AddForce( (Vector3)Random.insideUnitCircle * RandomForce, ForceMode.Impulse ); 
	}

	void OnCollisionEnter( Collision collision )
	{
		LocalRigidbody.AddForce( collision.contacts[0].normal * Force, ForceMode.Impulse ); 
		RotationSpeed.z = RotationSpeed.z * -1;	 
	} 

	void OnTriggerEnter( Collider collider )
	{
		if( collider.tag == "Wood" )
		{
			//print ( "Trigger Enter" );
			Transform Temp = FirePooling.ActivatePoolingObj();
			Temp.position = LocalTransform.position;
			Temp.parent = collider.transform;
		}
	}

	IEnumerator Death()
	{
		yield return new WaitForSeconds( TimeTillDeath );
		PartSystem.enableEmission = false;
		this.enabled = false;
		collider.enabled = false;
	}
}

