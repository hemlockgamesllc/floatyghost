﻿using UnityEngine;
using System.Collections;

public class ExplodeAfterTime : KillAfterTime {

	public float Radius;
	public float Force;
	public LayerMask ExplosionMask;
	
	// Update is called once per frame
	public override IEnumerator  WaitAndDie () 
	{
		yield return new WaitForSeconds( WaitTime );

		WhichObject.collider.enabled = false;
		Destroy ( WhichObject );

		Explosion.CreateExplosion( Radius, Force, WhichObject.transform.position, ExplosionMask );
	}
}
