﻿using UnityEngine;
using System.Collections;

public class PlayerInteraction : MonoBehaviour {

	public LayerMask Mask;
	public float Radius;
	public float Force;

	private float ChargeStartTime;

	public float MaxChargeTime;

	public float ExplosionRangeMin;
	public float ExplosionRangeMax;

	public Camera MainCamera;

	// Use this for initialization
	void Start () {
		//MainCamera = Camera.main;
	}  
	  
	// Update is called once per frame
	void Update () {
	
		if( Input.GetMouseButtonDown( 0 ) )
		{
			ChargeStartTime = Time.time;
			StartCoroutine( "MaxChargeTimer" );
		}

		if( Input.GetMouseButtonUp( 0 ) )
		{
			Detonate( (Time.time - ChargeStartTime) / MaxChargeTime );
		}
	} 

	private void Detonate( float PercentageOfMaxTime )
	{
		StopCoroutine( "MaxChargeTimer" ); 

		Explosion.CreateExplosion( Radius, Force, (Vector3)MainCamera.ScreenToWorldPoint( new Vector3( Input.mousePosition.x , Input.mousePosition.y, 2) ), Mask);
	}

	IEnumerator MaxChargeTimer()
	{
		yield return new WaitForSeconds( MaxChargeTime );

		Detonate( 1f );
	}

} 
