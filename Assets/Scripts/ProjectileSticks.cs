﻿using UnityEngine;
using System.Collections;

public class ProjectileSticks : MonoBehaviour {

	void OnCollisionEnter(Collision collision) {

		if (collision.relativeVelocity.magnitude < 1)
		{
			foreach (ContactPoint contact in collision.contacts) {
				//Debug.DrawRay(contact.point, contact.normal, Color.white);
				if( contact.otherCollider.tag == "Wood" )
				{
					collider.enabled = false;
					rigidbody.velocity = Vector3.zero;
					//rigidbody.active = false;
					 
					Vector3 LocalScale = transform.localScale;
					transform.parent = contact.otherCollider.transform;
					transform.localScale = LocalScale;
				}
			}
		}
		 
	}
}
