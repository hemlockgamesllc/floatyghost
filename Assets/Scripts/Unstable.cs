﻿using UnityEngine;
using System.Collections;

public class Unstable : MonoBehaviour {

	public float MinCollisionForceToExplode;

	void OnCollisionEnter(Collision collision) {
		
		if (collision.relativeVelocity.magnitude > MinCollisionForceToExplode)
		{
			gameObject.GetComponent<ExplosionReact>().ExplosionReaction() ;
		}
		
	}
}
