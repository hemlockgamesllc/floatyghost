﻿using UnityEngine;
using System.Collections;

public class ExplosiveBox :  ExplosionReact {
	
	public float FORCE = .1f;
	public LayerMask WhichObjectLayers;
	public float Range =5f;
	public GameObject BallPrefab;
	public float WaitAndExplode;

	private bool FirstExplosion = true;
	
	public Material ApplyMatOnExp;
	public GameObject ArtObj;
	  
	public override void ExplosionReaction()
	{ 
		if( FirstExplosion )
			StartCoroutine( Reaction() );
	}
	private IEnumerator Reaction()
	{
		Material[] CurrentMaterialsRef = ArtObj.renderer.materials;
		Material[] TempMaterials = new Material[CurrentMaterialsRef.Length + 1];  
		for( int q = 0 ; q < CurrentMaterialsRef.Length ; q++ )
		{
			TempMaterials[q] = CurrentMaterialsRef[q];
		}
		TempMaterials[ CurrentMaterialsRef.Length ] = ApplyMatOnExp;
		ArtObj.renderer.materials = TempMaterials;

		FirstExplosion = false;

		yield return new WaitForSeconds( WaitAndExplode );
		
		//print( "POP" ); 
		
		collider.enabled = false;
		Destroy( gameObject );
		
		for( int v = 0; v < transform.childCount ; v++ )
		{
			Instantiate( BallPrefab, transform.GetChild(v).position, Quaternion.identity );
		}
		
		Explosion.CreateExplosion( Range, FORCE, transform.position, WhichObjectLayers );


	}   
	 
} 