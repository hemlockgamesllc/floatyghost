﻿using UnityEngine;
using System.Collections;

public class SpawnObstructions : MonoBehaviour {

	public GameObject[] Obstructions;


	// Use this for initialization
	void Start () {
		//StartCoroutine( StartTimed() ) ;
	}

	 
	
	float DistanceTraveled = 0;
	void FixedUpdate()
	{
		 
		DistanceTraveled += GroundControl.MovementVector.y * Time.deltaTime;
		if( DistanceTraveled <= -.075f )
		{
			Instantiate( Obstructions[Random.Range(0,Obstructions.Length )], new Vector3( 0f, 7f, 0f ), Quaternion.identity  );
			DistanceTraveled = 0;
		}

	}

}
