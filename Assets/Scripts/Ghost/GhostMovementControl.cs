﻿using UnityEngine;
using System.Collections;

public class GhostMovementControl : MonoBehaviour {

	private float TargetHorizontal = 0;
	private Vector3 TempPosition;
	public Camera MainCamera; 

	private static readonly float MaxSpeed = 2.0f;
	private float CurrentMaxSpeed = MaxSpeed;
	private static readonly float MaxAccelerationTime = 0.3f;
	private float CurrentAccelerationTime = MaxAccelerationTime;
	private int NumberOfGravesCurrentlyIn = 0;
	private static GhostMovementControl StaticGhostReference;

	//private readonly float MovementSpeedRight = 0.035f;
	//private readonly float MovementSpeedLeft = -0.035f;

	private Vector3 Velocity;

	void Start()
	{
		StaticGhostReference = this;
	}

	// Use this for initialization 
	void Update () {
		if( Input.GetMouseButton( 0 ) )
		{
			TempPosition = MainCamera.ScreenToWorldPoint( Input.mousePosition );
			TempPosition.z = 0;
			TempPosition.y = transform.position.y;
				TargetHorizontal = TempPosition.x; 
		}   
	}    
	// Update is called once per frame
	void FixedUpdate () {  
		  
		transform.position  = Vector3.SmoothDamp( transform.position, TempPosition, ref Velocity, 0.3f, CurrentMaxSpeed );
		/*
		if( transform.position.x > TargetHorizontal )
			transform.position += new Vector3( MovementSpeedBackwards, 0f, 0f);
		else
			transform.position += new Vector3( MovementSpeed, 0f, 0f);
			*/
	}

	public static int GhostGraves
	{
		get{
			return StaticGhostReference.NumberOfGravesCurrentlyIn;
		}
		set{
			StaticGhostReference.NumberOfGravesCurrentlyIn = value;
			if ( StaticGhostReference.NumberOfGravesCurrentlyIn > 0 )
			{
				StaticGhostReference.CurrentMaxSpeed = MaxSpeed * 0.5f;
				StaticGhostReference.CurrentAccelerationTime = MaxAccelerationTime * 2f;
			}
			else if ( StaticGhostReference.NumberOfGravesCurrentlyIn <= 0 )
			{
				StaticGhostReference.CurrentMaxSpeed = MaxSpeed ;
				StaticGhostReference.CurrentAccelerationTime = MaxAccelerationTime;
			}

		}
	}
}
