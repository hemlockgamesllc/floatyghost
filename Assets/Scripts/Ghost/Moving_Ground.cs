﻿using UnityEngine;
using System.Collections;

public class Moving_Ground : Moving {

	private Vector3 GroundReset = new Vector3( 0f, 20f, 0f );

	// Update is called once per frame
	void FixedUpdate () {
		transform.position +=  GroundControl.MovementVector;

		if( transform.position.y <= -7f )
		{
			transform.position += GroundReset;
		} 
	}
}
