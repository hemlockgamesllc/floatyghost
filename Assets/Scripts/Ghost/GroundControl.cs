﻿using UnityEngine;
using System.Collections;

public class GroundControl : MonoBehaviour {

	public float StartSpeed = -0.01f;
	public float TimeTillMaxSpeed;
	public AnimationCurve CurveToMaxSpeed ;
	
	public static Vector3 MovementVector = Vector3.zero;

	void Start()
	{
		StartCoroutine( AdjustSpeedOverTime() );
	}

	IEnumerator AdjustSpeedOverTime()
	{
		MovementVector.y =  StartSpeed ;
		float StartTime = Time.time;

		while( true )
		{
			yield return new WaitForSeconds( 3f );
			MovementVector.y =  StartSpeed * CurveToMaxSpeed.Evaluate( (Time.time - StartTime)/TimeTillMaxSpeed  );
			//print ( MovementVector.y );
		}
	}

}
