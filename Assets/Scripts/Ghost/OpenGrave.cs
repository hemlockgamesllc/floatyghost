﻿using UnityEngine;
using System.Collections;

public class OpenGrave : MonoBehaviour {

	private bool PlayerInsideGrave = false;

	void OnTriggerEnter2D( Collider2D _collider )
	{
		if( _collider.tag == "Player" && !PlayerInsideGrave )
		{
			print ( "OnTriggerEnter" );
			GhostMovementControl.GhostGraves++;
			PlayerInsideGrave = true;
		}
	}
	
	void OnTriggerExit2D( Collider2D _collider )
	{
		print ( "OnTriggerExit" );
		if( _collider.tag == "Player" && PlayerInsideGrave )
		{
			GhostMovementControl.GhostGraves--;
			PlayerInsideGrave = false;
		}
	}
}
