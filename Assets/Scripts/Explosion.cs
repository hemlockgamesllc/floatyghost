﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public GameObject ExplosionPrefab;
	private static GameObject StaticExplosionPrefab;

	public float UpBiasMultiplier;
	private static float StaticUpBiasMultiplier;

	private static ExplosionReact ExpReactScript;
	//private static Explosion StatitExplosionScript;

	void Start()
	{
		StaticExplosionPrefab = ExplosionPrefab; 
		StaticUpBiasMultiplier = UpBiasMultiplier;
		//StatitExplosionScript = this;
	}
	
	public static void CreateExplosion( float Radius, float Force, Vector3 Origin, LayerMask WhichObjectLayers )
	{
		Collider[] hitColliders = Physics.OverlapSphere(Origin, Radius, WhichObjectLayers );
		int i = 0;
		while (i < hitColliders.Length) {
			//hitColliders[i].rigidbody.AddForce( (Vector3.Normalize(hitColliders[i].transform.position - Origin)) * Force, ForceMode.Impulse ); 
			
			//hitColliders[i].rigidbody.AddForce( Vector3.up * Force * StaticUpBiasMultiplier, ForceMode.Impulse ); 
			
			hitColliders[i].rigidbody.AddExplosionForce(Force, Origin, 0, StaticUpBiasMultiplier);
			
			ExpReactScript = null;
			ExpReactScript = hitColliders[i].GetComponent<ExplosionReact>();
			if( ExpReactScript != null )
			{
				ExpReactScript.ExplosionReaction() ;
			}

			i++;
		} 

		
		GameObject Temp = (GameObject) Instantiate( StaticExplosionPrefab, Origin, Quaternion.identity );
		Vector3 NewScale = new Vector3( Radius , Radius  , 1 );
		Temp.transform.localScale = NewScale;
	} 

}   