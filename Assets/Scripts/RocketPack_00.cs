﻿using UnityEngine;
using System.Collections;

public class RocketPack_00 : ExplosionReact {


	public float TimeBetweenLaunches;
	public int TotalLaunches;
	public float PushLauncherForce;
	public GameObject ProjectilePrefab;

	private Transform LocalTransform;
	private Rigidbody LocalRigidbody;

	public GameObject[] DestroyWhenDoneLaunching;

	private bool FirstExplosion = true;

	public Material ApplyMatOnExp;
	public GameObject ArtObj;

	// Use this for initialization
	void Start () {
		LocalTransform = transform;
		LocalRigidbody = rigidbody;
	}
	
	// Update is called once per frame
	public override void ExplosionReaction()
	{
		if( FirstExplosion )
			StartCoroutine( Reaction() );
	}

	private IEnumerator Reaction()
	{
		FirstExplosion = false;

		Material[] CurrentMaterialsRef = ArtObj.renderer.materials;
		Material[] TempMaterials = new Material[CurrentMaterialsRef.Length + 1];  
		for( int q = 0 ; q < CurrentMaterialsRef.Length ; q++ )
		{
			TempMaterials[q] = CurrentMaterialsRef[q];
		}
		TempMaterials[ CurrentMaterialsRef.Length ] = ApplyMatOnExp;
		ArtObj.renderer.materials = TempMaterials;
		 
		int LaunchCount = 0;
		while( LaunchCount < TotalLaunches )
		{
			yield return new WaitForSeconds( TimeBetweenLaunches );
			LaunchProjectile( LaunchCount );
			LaunchCount++;
		} 

		for( int i = 0;  i < DestroyWhenDoneLaunching.Length ; i++ )
		{
			Destroy ( DestroyWhenDoneLaunching[i] );
		}

	}   
	  
	private void LaunchProjectile( int Count )
	{
		// push self down from launch
		LocalRigidbody.AddForce(  LocalTransform.up * PushLauncherForce, ForceMode.Impulse ); 
		Transform CurrentChild = LocalTransform.GetChild( Count % LocalTransform.childCount );
		Instantiate( ProjectilePrefab, CurrentChild.position + (CurrentChild.up * 0.3f ) , CurrentChild.rotation );
	}
}
 