﻿using UnityEngine;
using System.Collections;

public class PinWheel :  ExplosionReact {
	
	public GameObject[] ObjectsToDestroy;
	public Transform RightArmSpawn;
	public Transform LeftArmSpawn;
	public GameObject PrefabToSpawn;
	  
	private bool FirstExplosion = true;

	public override void ExplosionReaction()
	{ 
		if( FirstExplosion )
		{
			FirstExplosion = false;
			
			CapsuleCollider[] Colliders = gameObject.GetComponents<CapsuleCollider>();

			Colliders[0].enabled = false;
			Colliders[1].enabled = false;

			Instantiate( PrefabToSpawn, RightArmSpawn.position, RightArmSpawn.rotation );
			Instantiate( PrefabToSpawn, LeftArmSpawn.position, LeftArmSpawn.rotation );

			for( int i = 0 ; i < ObjectsToDestroy.Length ; i++ )
			{
				Destroy( ObjectsToDestroy[i] );
			}
			  
		}
	}   
	 
} 