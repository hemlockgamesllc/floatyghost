﻿using UnityEngine;
using System.Collections;

public class KillAfterTime : MonoBehaviour {

	public float WaitTime;
	public GameObject WhichObject;

	// Use this for initialization
	void Start () {
		StartCoroutine( WaitAndDie() );
	}
	
	// Update is called once per frame
	public virtual IEnumerator  WaitAndDie () 
	{
		yield return new WaitForSeconds( WaitTime );

		Destroy ( WhichObject );
	}
}
