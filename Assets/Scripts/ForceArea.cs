﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ForceArea : MonoBehaviour, InformWhenDead {

	private List<GameObject> ObjectsInsideMe;
	private Transform LocalTransform;
	public float Force;

	void Start()
	{
		ObjectsInsideMe = new List<GameObject>();
		LocalTransform = transform;
	} 

	void OnTriggerEnter (Collider collision)
	{
		ObjectsInsideMe.Add( collision.gameObject );
		collision.gameObject.GetComponent<InformBecauseDieing>().InformMe( this );
		//print ( "Trigger! " + collision.name + " Size: " + ObjectsInsideMe.Count);
	}
	
	void OnTriggerExit (Collider collision)
	{
		ObjectsInsideMe.Remove( collision.gameObject );
		collision.gameObject.GetComponent<InformBecauseDieing>().DontInformMe( this );
		//print ( "Leave! " + collision.name + " Size: " + ObjectsInsideMe.Count);
	}

	void InformWhenDead.ThisObjectDied( GameObject Obj )
	{
		ObjectsInsideMe.Remove( Obj );
		//print ( "Leave! " + Obj + " Size: " + ObjectsInsideMe.Count);
		//print ( "I have been informed that " + Obj.name + " has died!" );
	}

	void FixedUpdate()
	{
		
		for( int i = 0; i < ObjectsInsideMe.Count ; i++ )
		{
			ObjectsInsideMe[i].rigidbody.AddForce( LocalTransform.up * Force );
		}
	}

}
